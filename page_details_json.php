[{
	'date': '25-05-2015',
	'logged_in_time': 123456789(timestamp),  // At what time user logged in
	logged_out_time: 123456789(timestamp), // At what time user logged out
	total_time: 120(In seconds),           // total time between login and logout
	actions: [
		{
			'pageurl': '/orders/create',
			'time': 50(In seconds),
			'type': 'page'
		},
		{
			'pageurl': 'accounts/index/deactivateaccount',
			'time': 0,
			'type': 'ajax'
		},
		{
			'pageurl': '/inventory/add',
			'time': 70(In seconds),
			'type': 'page'
		}
	]
},{
	date: '28-05-2015',
	logged_in_time: 123456789(timestamp),  // At what time user logged in
	logged_out_time: 123456789(timestamp), // At what time user logged out
	total_time: 120(In seconds),           // total time between login and logout
	actions: [
		{
			'pageurl': '/orders/update',
			'time': 40(In seconds),
			'type': 'page'
		},
		{
			'pageurl': '/orders/search',
			'time': 50(In seconds),
			'type': 'page'
		},
		{
			'pageurl': '/inventory/edit',
			'time': 30(In seconds),
			'type': 'page'
		}
	]
}
]